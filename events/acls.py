import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_photo(city, state):
    url = "https://api.pexels.com/v1/search?query="
    headers = {"Authorization": PEXELS_API_KEY}
    query = str(city) + " " + str(state)

    r = requests.get(url + query, headers=headers)
    response = json.loads(r.content)
    url = response["photos"][0]["url"]
    return url

def get_weather_data(city, state):
    url = f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state},unitedstates&limit=5&appid={OPEN_WEATHER_API_KEY}'
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    r = requests.get(url, headers=headers)
    coordinates = json.loads(r.content)
    # print(coordinates)
    if len(coordinates) == 0:
        return None
    lat = coordinates[0]["lat"]
    lon = coordinates[0]["lon"]

    url = f'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial'
    r = requests.get(url)
    weather = json.loads(r.content)
    # print(weather)
    return {
        "temp": weather["main"]['temp'],
        "description": weather["weather"][0]["description"],
    }

# {
#   "total_results": 10000,
#   "page": 1,
#   "per_page": 1,
#   "photos": [
#     {
#       "id": 3573351,
#       "width": 3066,
#       "height": 3968,
#       "url": "https://www.pexels.com/photo/trees-during-day-3573351/",
#       "photographer": "Lukas Rodriguez",
#       "photographer_url": "https://www.pexels.com/@lukas-rodriguez-1845331",
#       "photographer_id": 1845331,
#       "avg_color": "#374824",
#       "src": {
#         "original": "https://images.pexels.com/photos/3573351/pexels-photo-3573351.png",
#         "large2x": "https://images.pexels.com/photos/3573351/pexels-photo-3573351.png?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
#         "large": "https://images.pexels.com/photos/3573351/pexels-photo-3573351.png?auto=compress&cs=tinysrgb&h=650&w=940",
#         "medium": "https://images.pexels.com/photos/3573351/pexels-photo-3573351.png?auto=compress&cs=tinysrgb&h=350",
#         "small": "https://images.pexels.com/photos/3573351/pexels-photo-3573351.png?auto=compress&cs=tinysrgb&h=130",
#         "portrait": "https://images.pexels.com/photos/3573351/pexels-photo-3573351.png?auto=compress&cs=tinysrgb&fit=crop&h=1200&w=800",
#         "landscape": "https://images.pexels.com/photos/3573351/pexels-photo-3573351.png?auto=compress&cs=tinysrgb&fit=crop&h=627&w=1200",
#         "tiny": "https://images.pexels.com/photos/3573351/pexels-photo-3573351.png?auto=compress&cs=tinysrgb&dpr=1&fit=crop&h=200&w=280"
#       },
#       "liked": false,
#       "alt": "Brown Rocks During Golden Hour"
#     }
#   ],
#   "next_page": "https://api.pexels.com/v1/search/?page=2&per_page=1&query=nature"
# }
